package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.repository.AddressBookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by prav on 22/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AddAddressBookValidatorTest {

    @Mock
    private AddressBookRepository addressBookRepository;

    @InjectMocks
    private AddAddressBookValidator validator = new AddAddressBookValidator(addressBookRepository);

    @Test
    public void validate_should_return_true_when_valid() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("Test").build();
        assertThat(validator.validate(addressBook), is(true));
    }

    @Test
    public void validate_should_return_false_when_address_book_name_is_empty_string() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("").build();
        assertThat(validator.validate(addressBook), is(false));
    }

    @Test
    public void validate_should_return_false_when_address_book_name_is_null() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName(null).build();
        assertThat(validator.validate(addressBook), is(false));
    }

    @Test
    public void validate_should_return_false_when_address_book_has_spaces_only() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("    ").build();
        assertThat(validator.validate(addressBook), is(false));
    }

    @Test
    public void validate_should_return_false_when_address_book_with_name_already_exists() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("Test").build();
        when(addressBookRepository.selectByName("Test")).thenReturn(addressBook);
        assertThat(validator.validate(addressBook), is(false));
    }
}
