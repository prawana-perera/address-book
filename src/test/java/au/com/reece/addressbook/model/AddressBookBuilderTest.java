package au.com.reece.addressbook.model;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by prav on 22/01/2017.
 */
public class AddressBookBuilderTest {

    @Test
    public void build_should_return_an_address_book() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("Test").build();
        assertThat(addressBook, is(notNullValue()));
        assertThat(addressBook.getName(), is("Test"));

    }
}
