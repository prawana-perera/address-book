package au.com.reece.addressbook.model;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by prav on 22/01/2017.
 */
public class ContactBuilderTest {

    @Test
    public void build_should_return_a_contact() {
        Contact contact = Contact.contactBuilder().withName("Test").withPhoneNumber("8888").build();

        assertThat(contact, is(notNullValue()));
        assertThat(contact.getName(), is("Test"));
        assertThat(contact.getPhoneNumber(), is("8888"));
    }
}
