package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.AddressBookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by prav on 22/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RemoveContactValidatorTest {

    private static final AddressBook ADDRESS_BOOK = AddressBook.addressBookBuilder().withName("Test").build();
    private static final Contact CONTACT = Contact.contactBuilder().withName("Prav").withPhoneNumber("8888").build();

    @Mock
    private AddressBookRepository addressBookRepository;

    @InjectMocks
    private RemoveContactValidator validator = new RemoveContactValidator(ADDRESS_BOOK.getName(), addressBookRepository);

    @Test
    public void validate_should_return_false_when_address_book_does_not_exist() {
        when(addressBookRepository.selectByName(ADDRESS_BOOK.getName())).thenReturn(null);
        assertThat(validator.validate(CONTACT), is(false));
    }

    @Test
    public void validate_should_return_false_when_contact_does_not_exist_in_address_book() {
        when(addressBookRepository.selectByName(ADDRESS_BOOK.getName())).thenReturn(ADDRESS_BOOK);
        assertThat(validator.validate(CONTACT), is(false));
    }

    @Test
    public void validate_should_return_true_when_valid() {
        AddressBook addressBook2 = AddressBook.addressBookBuilder().withName("Book2").build();
        addressBook2.getContacts().add(CONTACT);

        when(addressBookRepository.selectByName(ADDRESS_BOOK.getName())).thenReturn(addressBook2);
        assertThat(validator.validate(CONTACT), is(true));
    }
}
