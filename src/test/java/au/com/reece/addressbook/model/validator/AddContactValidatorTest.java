package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.AddressBookRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by prav on 22/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AddContactValidatorTest {

    private static final AddressBook ADDRESS_BOOK = AddressBook.addressBookBuilder().withName("Test").build();

    @Mock
    private AddressBookRepository addressBookRepository;

    @InjectMocks
    private AddContactValidator validator = new AddContactValidator(ADDRESS_BOOK.getName(), addressBookRepository);

    @Test
    public void validate_should_return_true_when_valid() {
        when(addressBookRepository.selectByName(ADDRESS_BOOK.getName())).thenReturn(ADDRESS_BOOK);
        Contact contact = Contact.contactBuilder().withName("Prav").withPhoneNumber("8888").build();
        assertThat(validator.validate(contact), is(true));
    }

    @Test
    public void validate_should_return_false_when_contact_details_are_missing() {
        Contact contact = Contact.contactBuilder().withName("").withPhoneNumber("").build();
        assertThat(validator.validate(contact), is(false));
    }

    @Test
    public void validate_should_return_false_when_target_address_book_does_not_exist() {
        when(addressBookRepository.selectByName(ADDRESS_BOOK.getName())).thenReturn(null);
        Contact contact = Contact.contactBuilder().withName("Prav").withPhoneNumber("8888").build();
        assertThat(validator.validate(contact), is(false));
    }

}
