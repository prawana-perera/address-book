package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by prav on 21/01/2017.
 */
public class AddressBookRepositoryImplTest {

    private AddressBookRepositoryImpl repository = new AddressBookRepositoryImpl();

    @Before
    public void setUp() {
        InMemoryDatabase.ADDRESS_BOOKS.clear();
    }

    @Test
    public void add_should_add_an_address_book() {

        String name = "TestBook";
        AddressBook addressBook = AddressBook.addressBookBuilder().withName(name).build();

        repository.add(addressBook);

        AddressBook addressBookInDb = InMemoryDatabase.ADDRESS_BOOKS.get(name);
        assertThat(addressBookInDb, is(notNullValue()));
        assertThat(addressBookInDb.getName(), is(name));
    }

    @Test
    public void selectByName_should_return_address_book() {
        String name = "TestBook";
        InMemoryDatabase.ADDRESS_BOOKS.put(name, AddressBook.addressBookBuilder().withName(name).build());

        AddressBook addressBook = repository.selectByName(name);
        assertThat(addressBook, is(notNullValue()));
        assertThat(addressBook.getName(), is(name));
    }

    @Test
    public void selectByName_should_return_null_when_address_book_with_name_does_not_exist() {
        AddressBook addressBook = repository.selectByName("TestBook");
        assertThat(addressBook, is(nullValue()));
    }

    @Test
    public void addContact_should_add_contact_to_address_book() {

        AddressBook addressBook = AddressBook.addressBookBuilder().withName("Test Address Book").build();
        InMemoryDatabase.ADDRESS_BOOKS.put(addressBook.getName(), addressBook);
        assertThat(addressBook.getContacts(), hasSize(0));

        Contact contact = Contact.contactBuilder()
                .withName("Prav P")
                .withPhoneNumber("8888 9999")
                .build();

        repository.addContact(addressBook.getName(), contact);

        addressBook = InMemoryDatabase.ADDRESS_BOOKS.get(addressBook.getName());
        assertThat(addressBook.getContacts(), hasSize(1));

    }

    @Test
    public void removeContact_should_remove_the_contact() {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName("Test Address Book").build();
        Contact contact = Contact.contactBuilder()
                .withName("Prav P")
                .withPhoneNumber("8888 9999")
                .build();
        addressBook.getContacts().add(contact);
        InMemoryDatabase.ADDRESS_BOOKS.put(addressBook.getName(), addressBook);

        assertThat(addressBook.getContacts(), hasSize(1));

        repository.removeContact(addressBook.getName(), contact);
        addressBook = InMemoryDatabase.ADDRESS_BOOKS.get(addressBook.getName());
        assertThat(addressBook.getContacts(), hasSize(0));
    }
}
