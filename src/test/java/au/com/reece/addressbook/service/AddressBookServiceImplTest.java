package au.com.reece.addressbook.service;

import au.com.reece.addressbook.common.Validator;
import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.model.validator.ValidationFactory;
import au.com.reece.addressbook.repository.AddressBookRepository;
import au.com.reece.addressbook.service.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

/**
 * Created by prav on 21/01/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class AddressBookServiceImplTest {

    @InjectMocks
    private AddressBookServiceImpl service = new AddressBookServiceImpl();

    @Mock
    private AddressBookRepository addressBookRepository;

    @Mock
    private Validator<AddressBook> addAddressBookValidator;

    @Mock
    private Validator<Contact> addContactValidator;

    @Mock
    private Validator<Contact> removeContactValidator;

    @Mock
    private ValidationFactory validationFactory;

    @Before
    public void setUp() {
        when(validationFactory.createValidatorForAddAddressBook()).thenReturn(addAddressBookValidator);
        when(validationFactory.createValidatorForAddContact(any())).thenReturn(addContactValidator);
        when(validationFactory.createValidatorForRemoveContact(any())).thenReturn(removeContactValidator);
    }

    @Test
    public void createAddressBook_should_create_an_address_book() {

        when(addAddressBookValidator.validate(any())).thenReturn(true);

        String name = "TetBook";
        service.createAddressBook(name);
        verify(addressBookRepository, times(1)).add(any(AddressBook.class));
    }

    @Test
    public void createAddressBook_should_not_add_address_book_when_validation_fails() {

        try {
            when(addAddressBookValidator.validate(any())).thenReturn(false);
            service.createAddressBook("");
            fail("Expected to throw a ServiceException.");
        } catch (ServiceException e) {
            verify(addressBookRepository, never()).add(any(AddressBook.class));
        }
    }

    @Test
    public void createContact_should_add_contact() {

        Contact contact = Contact.contactBuilder()
                .withName("Prav P")
                .withPhoneNumber("8888 8888")
                .build();

        when(addContactValidator.validate(contact)).thenReturn(true);

        service.createContact("Test", contact);
        verify(addressBookRepository, times(1)).addContact("Test", contact);
    }

    @Test
    public void createContact_should_not_add_contact_when_validation_fails() {

        Contact contact = Contact.contactBuilder()
                .withName("")
                .withPhoneNumber("")
                .build();
        when(addContactValidator.validate(contact)).thenReturn(false);

        try {
            service.createContact("Test", contact);
            fail("Expected to throw a ServiceException.");
        } catch (ServiceException e) {
            verify(addressBookRepository, never()).addContact("Test", contact);
        }
    }

    @Test
    public void removeContact_should_remove_the_contact() {
        Contact contact = Contact.contactBuilder()
                .withName("Prav")
                .withPhoneNumber("222")
                .build();
        when(removeContactValidator.validate(contact)).thenReturn(true);

        service.removeContact("Test", contact);
        verify(addressBookRepository, times(1)).removeContact("Test", contact);
    }

    @Test
    public void removeContact_should_not_remove_the_contact_when_validation_fails() {
        Contact contact = Contact.contactBuilder()
                .withName("Prav")
                .withPhoneNumber("222")
                .build();
        when(removeContactValidator.validate(contact)).thenReturn(false);

        try {
            service.removeContact("Test", contact);
            fail("Expected to throw a ServiceException.");
        } catch (ServiceException e) {
            verify(addressBookRepository, never()).removeContact("Test", contact);
        }
    }

    @Test
    public void getAddressBook_should_return_address_book_with_contacts() {
        AddressBook addressBookSrc = AddressBook.addressBookBuilder().withName("Test").build();
        Contact contactSrc = Contact.contactBuilder().withName("Prav").withPhoneNumber("111").build();
        addressBookSrc.getContacts().add(contactSrc);

        when(addressBookRepository.selectByName(addressBookSrc.getName())).thenReturn(addressBookSrc);

        AddressBook addressBook = service.getAddressBook(addressBookSrc.getName());
        assertThat(addressBook, is(notNullValue()));
        assertThat(addressBook.getName(), is(addressBookSrc.getName()));
        assertThat(addressBook.getContacts(), hasSize(1));
        assertThat(addressBook.getContacts().get(0), is(contactSrc));
    }

    @Test
    public void getAddressBook_should_return_null_when_address_book_does_not_exist() {
        when(addressBookRepository.selectByName("Abc")).thenReturn(null);

        AddressBook addressBook = service.getAddressBook("Abc");
        assertThat(addressBook, is(nullValue()));
    }

    @Test
    public void getUniqueContacts_should_return_empty_list_when_address_books_do_not_exist() {
        when(addressBookRepository.selectByName(anyString())).thenReturn(null);
        assertThat(service.getUniqueContacts("aaa", "bbb", "ccc"), is(empty()));
    }

    @Test
    public void getUniqueContacts_should_return_list_of_unique_contacts() {
        Contact sue = Contact.contactBuilder().withName("Sue P").withPhoneNumber("8888 9999").build();
        Contact john = Contact.contactBuilder().withName("John S").withPhoneNumber("4444 4444").build();
        Contact steve = Contact.contactBuilder().withName("Steve M").withPhoneNumber("7777 6666").build();

        AddressBook vicCustomers = AddressBook.addressBookBuilder().withName("VIC Customers").build();
        vicCustomers.getContacts().add(sue);
        vicCustomers.getContacts().add(steve);
        vicCustomers.getContacts().add(john);

        AddressBook bulkOrderCustomers = AddressBook.addressBookBuilder().withName("Bulk Order Customers").build();
        bulkOrderCustomers.getContacts().add(john);

        when(addressBookRepository.selectByName(vicCustomers.getName())).thenReturn(vicCustomers);
        when(addressBookRepository.selectByName(bulkOrderCustomers.getName())).thenReturn(bulkOrderCustomers);

        List<Contact> uniqueContacts = service.getUniqueContacts(vicCustomers.getName(), bulkOrderCustomers.getName());

        assertThat(uniqueContacts, is(notNullValue()));
        assertThat(uniqueContacts, hasSize(3));
        assertThat(uniqueContacts, hasItem(sue));
        assertThat(uniqueContacts, hasItem(john));
        assertThat(uniqueContacts, hasItem(steve));
    }
}
