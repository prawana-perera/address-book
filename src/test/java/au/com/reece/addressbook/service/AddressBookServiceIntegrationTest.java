package au.com.reece.addressbook.service;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.InMemoryDatabase;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by prav on 22/01/2017.
 */
public class AddressBookServiceIntegrationTest {

    private AddressBookService service = new AddressBookServiceImpl();

    @Before
    public void setUp() {
        InMemoryDatabase.ADDRESS_BOOKS.clear();
    }

    @Test
    public void it_should_add_a_new_address_book() {

        String addressBookName = "VIC Customers";
        service.createAddressBook(addressBookName);

        AddressBook addressBookInDb = InMemoryDatabase.ADDRESS_BOOKS.get(addressBookName);
        assertThat(addressBookInDb, is(notNullValue()));
        assertThat(addressBookInDb.getName(), is(addressBookName));
    }

    @Test
    public void it_should_manage_multiple_address_books() {

        String addressBookNameVIC = "VIC Customers";
        service.createAddressBook(addressBookNameVIC);

        String addressBookNameNSW = "NSW Customers";
        service.createAddressBook(addressBookNameNSW);

        assertThat(InMemoryDatabase.ADDRESS_BOOKS.size(), is(2));
    }

    @Test
    public void it_should_add_new_contact() {
        seedAddressBookWithContacts("VIC Customers");

        Contact contact = Contact.contactBuilder()
                .withName("Prav P")
                .withPhoneNumber("8888 9999")
                .build();

        service.createContact("VIC Customers", contact);

        AddressBook addressBook = InMemoryDatabase.ADDRESS_BOOKS.get("VIC Customers");
        assertThat(addressBook.getContacts(), hasSize(1));

        Contact contactAdded = addressBook.getContacts().get(0);
        assertThat(contactAdded.getName(), is(contact.getName()));
        assertThat(contactAdded.getPhoneNumber(), is(contact.getPhoneNumber()));

    }

    @Test
    public void it_should_remove_a_contact() {

        Contact contact = Contact.contactBuilder()
                .withName("Prav P")
                .withPhoneNumber("8888 9999")
                .build();
        seedAddressBookWithContacts("VIC Customers", contact);

        service.removeContact("VIC Customers", contact);

        AddressBook addressBook = InMemoryDatabase.ADDRESS_BOOKS.get("VIC Customers");
        assertThat(addressBook.getContacts(), hasSize(0));
    }

    @Test
    public void it_should_get_address_book_details_with_contacts() {

        Contact contact = Contact.contactBuilder().withName("Prav P").withPhoneNumber("8888 9999").build();
        seedAddressBookWithContacts("VIC Customers", contact);

        AddressBook addressBook = service.getAddressBook("VIC Customers");
        assertThat(addressBook, is(notNullValue()));
        assertThat(addressBook.getName(), is("VIC Customers"));
        assertThat(addressBook.getContacts(), hasSize(1));
        assertThat(addressBook.getContacts().get(0), is(contact));
    }

    @Test
    public void it_should_get_unique_contacts_across_multiple_address_books() {

        Contact sue = Contact.contactBuilder().withName("Sue P").withPhoneNumber("8888 9999").build();
        Contact john = Contact.contactBuilder().withName("John S").withPhoneNumber("4444 4444").build();
        Contact steve = Contact.contactBuilder().withName("Steve M").withPhoneNumber("7777 6666").build();

        AddressBook vicCustomers = seedAddressBookWithContacts("VIC Customers", sue, john, steve);
        AddressBook bulkOrderCustomers = seedAddressBookWithContacts("Bulk Order Customers", sue, steve);

        List<Contact> uniqueContacts = service.getUniqueContacts(vicCustomers.getName(), bulkOrderCustomers.getName());

        assertThat(uniqueContacts, is(notNullValue()));
        assertThat(uniqueContacts, hasSize(3));
        assertThat(uniqueContacts, hasItem(sue));
        assertThat(uniqueContacts, hasItem(john));
        assertThat(uniqueContacts, hasItem(steve));
    }

    private AddressBook seedAddressBookWithContacts(String addressBookName, Contact... contacts) {
        AddressBook addressBook = AddressBook.addressBookBuilder().withName(addressBookName).build();
        Stream.of(contacts).forEach(contact -> addressBook.getContacts().add(contact));
        InMemoryDatabase.ADDRESS_BOOKS.put(addressBook.getName(), addressBook);

        return addressBook;
    }
}
