package au.com.reece.addressbook.service;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;

import java.util.List;

/**
 * Created by prav on 21/01/2017.
 */
public interface AddressBookService {

    void createAddressBook(String name);

    void createContact(String addressBookName, Contact contact);

    void removeContact(String addressBookName, Contact contact);

    AddressBook getAddressBook(String name);

    List<Contact> getUniqueContacts(String... addressBookNames);
}
