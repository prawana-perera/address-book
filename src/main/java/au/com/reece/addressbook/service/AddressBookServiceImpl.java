package au.com.reece.addressbook.service;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.model.validator.ValidationFactory;
import au.com.reece.addressbook.repository.AddressBookRepository;
import au.com.reece.addressbook.repository.AddressBookRepositoryImpl;
import au.com.reece.addressbook.service.exception.ServiceException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by prav on 21/01/2017.
 */
public class AddressBookServiceImpl implements AddressBookService {

    private AddressBookRepository addressBookRepository = new AddressBookRepositoryImpl();
    private ValidationFactory validationFactory = new ValidationFactory(addressBookRepository);

    public void createAddressBook(String name) {

        AddressBook addressBook = AddressBook.addressBookBuilder()
                .withName(name)
                .build();

        if (!validationFactory.createValidatorForAddAddressBook().validate(addressBook)) {
            throw new ServiceException();
        }

        addressBookRepository.add(addressBook);
    }

    @Override
    public void createContact(String addressBookName, Contact contact) {

        if (!validationFactory.createValidatorForAddContact(addressBookName).validate(contact)) {
            throw new ServiceException();
        }

        addressBookRepository.addContact(addressBookName, contact);
    }

    @Override
    public void removeContact(String addressBookName, Contact contact) {

        if (!validationFactory.createValidatorForRemoveContact(addressBookName).validate(contact)) {
            throw new ServiceException();
        }

        addressBookRepository.removeContact(addressBookName, contact);
    }

    @Override
    public AddressBook getAddressBook(String name) {
        return addressBookRepository.selectByName(name);
    }

    @Override
    public List<Contact> getUniqueContacts(String... addressBookNames) {

        return Stream.of(addressBookNames)
                .map(name -> Optional
                        .ofNullable(addressBookRepository.selectByName(name))
                        .map(addressBook -> addressBook.getContacts())
                        .orElse(Collections.emptyList()))
                .flatMap(contacts -> contacts.stream())
                .distinct()
                .collect(Collectors.toList());
    }

}
