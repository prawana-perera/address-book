package au.com.reece.addressbook.model;

import au.com.reece.addressbook.common.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by prav on 21/01/2017.
 */
public class Contact {

    private String name;
    private String phoneNumber;

    private Contact(ContactBuilder builder) {
        this.phoneNumber = builder.phoneNumber;
        this.name = builder.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        return new EqualsBuilder()
                .append(name, contact.name)
                .append(phoneNumber, contact.phoneNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(phoneNumber)
                .toHashCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static ContactBuilder contactBuilder() {
        return new ContactBuilder();
    }

    public static class ContactBuilder implements Builder<Contact> {

        private String name;
        private String phoneNumber;

        private ContactBuilder() {

        }

        public ContactBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ContactBuilder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        @Override
        public Contact build() {
            return new Contact(this);
        }
    }
}
