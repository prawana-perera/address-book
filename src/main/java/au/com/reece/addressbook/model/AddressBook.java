package au.com.reece.addressbook.model;

import au.com.reece.addressbook.common.Builder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prav on 21/01/2017.
 */
public class AddressBook {

    private String name;
    private List<Contact> contacts;

    private AddressBook(AddressBookBuilder builder) {
        this.contacts = new ArrayList<>();
        this.name = builder.name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AddressBook that = (AddressBook) o;

        return new EqualsBuilder()
                .append(name, that.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .toHashCode();
    }

    public static AddressBookBuilder addressBookBuilder() {
        return new AddressBookBuilder();
    }

    public static class AddressBookBuilder implements Builder<AddressBook> {

        private String name;

        private AddressBookBuilder() {
        }

        public AddressBookBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AddressBook build() {
            return new AddressBook(this);
        }
    }
}
