package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.common.Validator;
import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.AddressBookRepository;

/**
 * Created by prav on 22/01/2017.
 */
public class RemoveContactValidator implements Validator<Contact> {

    private String addressBookName;
    private AddressBookRepository addressBookRepository;

    public RemoveContactValidator(String addressBookName, AddressBookRepository addressBookRepository) {
        this.addressBookName = addressBookName;
        this.addressBookRepository = addressBookRepository;
    }

    @Override
    public boolean validate(Contact toValidate) {

        if (toValidate == null) {
            return false;
        }

        AddressBook addressBook = addressBookRepository.selectByName(this.addressBookName);

        if (addressBook == null) {
            return false;
        }

        if (!addressBook.getContacts().contains(toValidate)) {
            return false;
        }

        return true;
    }
}
