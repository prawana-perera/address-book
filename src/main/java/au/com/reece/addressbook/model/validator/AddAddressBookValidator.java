package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.common.Validator;
import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.repository.AddressBookRepository;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by prav on 22/01/2017.
 */
public class AddAddressBookValidator implements Validator<AddressBook> {

    private AddressBookRepository addressBookRepository;

    public AddAddressBookValidator(AddressBookRepository addressBookRepository) {
        this.addressBookRepository = addressBookRepository;
    }

    @Override
    public boolean validate(AddressBook toValidate) {
        if (toValidate == null || StringUtils.isBlank(toValidate.getName())) {
            return false;
        }

        if (addressBookRepository.selectByName(toValidate.getName()) != null) {
            return false;
        }

        return true;
    }
}
