package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.common.Validator;
import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.AddressBookRepository;

/**
 * Created by prav on 22/01/2017.
 */
public class ValidationFactory {

    private AddressBookRepository addressBookRepository;

    public ValidationFactory(AddressBookRepository addressBookRepository) {
        this.addressBookRepository = addressBookRepository;
    }

    public Validator<AddressBook> createValidatorForAddAddressBook() {
        return new AddAddressBookValidator(addressBookRepository);
    }

    public Validator<Contact> createValidatorForAddContact(String addressBookName) {
        return new AddContactValidator(addressBookName, addressBookRepository);
    }

    public Validator<Contact> createValidatorForRemoveContact(String addressBookName) {
        return new RemoveContactValidator(addressBookName, addressBookRepository);
    }
}
