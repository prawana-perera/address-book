package au.com.reece.addressbook.model.validator;

import au.com.reece.addressbook.common.Validator;
import au.com.reece.addressbook.model.Contact;
import au.com.reece.addressbook.repository.AddressBookRepository;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by prav on 22/01/2017.
 */
public class AddContactValidator implements Validator<Contact> {

    private String addressBookName;
    private AddressBookRepository addressBookRepository;

    public AddContactValidator(String addressBookName, AddressBookRepository addressBookRepository) {
        this.addressBookName = addressBookName;
        this.addressBookRepository = addressBookRepository;
    }

    @Override
    public boolean validate(Contact toValidate) {

        if (toValidate == null) {
            return false;
        }

        if (StringUtils.isBlank(toValidate.getName()) || StringUtils.isBlank(toValidate.getPhoneNumber())) {
            return false;
        }

        if (addressBookRepository.selectByName(this.addressBookName) == null) {
            return false;
        }

        return true;
    }
}
