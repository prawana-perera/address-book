package au.com.reece.addressbook.common;

/**
 * Created by prav on 21/01/2017.
 */
public interface Validator<T> {

    boolean validate(T toValidate);
}
