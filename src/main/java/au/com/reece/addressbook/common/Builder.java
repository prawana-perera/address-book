package au.com.reece.addressbook.common;

/**
 * Created by prav on 22/01/2017.
 */
public interface Builder<T> {

    T build();
}
