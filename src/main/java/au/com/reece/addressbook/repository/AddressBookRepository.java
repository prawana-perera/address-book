package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;

/**
 * Created by prav on 21/01/2017.
 */
public interface AddressBookRepository {
    void add(AddressBook addressBook);

    AddressBook selectByName(String name);

    void addContact(String addressBookName, Contact contact);

    void removeContact(String addressBookName, Contact contact);
}
