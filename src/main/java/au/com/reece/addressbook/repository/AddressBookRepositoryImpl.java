package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.model.AddressBook;
import au.com.reece.addressbook.model.Contact;

/**
 * Created by prav on 21/01/2017.
 */
public class AddressBookRepositoryImpl implements AddressBookRepository {

    @Override
    public void add(AddressBook addressBook) {
        InMemoryDatabase.ADDRESS_BOOKS.put(addressBook.getName(), addressBook);
    }

    @Override
    public AddressBook selectByName(String name) {
        return InMemoryDatabase.ADDRESS_BOOKS.get(name);
    }

    @Override
    public void addContact(String addressBookName, Contact contact) {
        this.selectByName(addressBookName).getContacts().add(contact);
    }

    @Override
    public void removeContact(String addressBookName, Contact contact) {
        this.selectByName(addressBookName).getContacts().remove(contact);
    }
}
