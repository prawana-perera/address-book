package au.com.reece.addressbook.repository;

import au.com.reece.addressbook.model.AddressBook;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by prav on 21/01/2017.
 */
public class InMemoryDatabase {

    public static Map<String, AddressBook> ADDRESS_BOOKS = new HashMap<>();
}
