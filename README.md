# Description #
This repository contains an example implementation of an Address Book application which has been developed using TDD techniques. The specified requirement for this application are:

```
As a Reece Branch Manager
I would like an address book application on my PC
So that I can keep track of my customer contacts

Acceptance Criteria
  - Address book will hold name and phone numbers of contact entries
  - Users should be able to add new contact entries
  - Users should be able to remove existing contact entries
  - Users should be able to print all contacts in an address book
  - Users should be able to maintain multiple address books
  - Users should be able to print a unique set of all contacts across multiple address books
```
My developed approach used TDD. I used the acceptance criteria above to write the tests first and let the tests shape the API of the application.

The functionality of the the application is demonstrated via acceptance (integration) tests based on the acceptance criteria above. In addition more unit test were added to test individual components in isolation, which allowed me to test boundary conditions and other cases not explicitly specified in the acceptance criteria.

For all acceptance criteria tests see the `AddressBookServiceIntegrationTest`.


### Application Design ###

The application design was kept simple. It employs a service class (AddressBookService) which is the exposed API to clients. The service implementation uses a repository class (AddressBookRepository) which abstracts the data operations. The repository implementation uses an in-memory datastore for demonstration purposes. In addition the business/model objects are encapsulated within their own classes and the business rule validations are delegated to separate validator classes.

The use of vast frameworks or libraries were avoided as it was not needed for this exercise. Only a small handful of libraries were used, mainly to drive tests (JUnit, Mockito). Gradle was used the build tool.

### How do I get set up? ###

In order to setup and run you will need:

* Java 8
* A internet connection
* If you are behind a corporate proxy, you may need to add proxy configuration (see Gradle documentation)

To setup and run:

* Clone this repository
* Open a terminal and change directory to where you have cloned it
* Run `./gradlew build` (Linux/Mac) or `gradlew build` (Windows)

The above will run all tests and demonstrate the execution of acceptance criteria as well as all other unit tests. Navigate to the project's `build/reports/tests/test` directory and open the `index.html` file in a browser to see the result of test executions.